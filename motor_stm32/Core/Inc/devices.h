/*
 * devices.h
 *
 *  Created on: 25 avr. 2022
 *      Author: Alain
 */

#ifndef INC_DEVICES_H_
#define INC_DEVICES_H_

#include "main.h"
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

#define L_FW_ROT 0
#define L_BW_ROT 1
#define R_FW_ROT 1
#define R_BW_ROT 0


#define N_US_SENSORS 2
#define N_IR_SENSORS 7
#define US_DELAY 200
#define IR_DELAY 50
#define	RANGE_LOG_DELAY 500
#define UART3_TIMEOUT 10
#define MAX_TRY_UART3 5
#define DYNAMIXEL_ARM_ID 0x06

#define ARM_BOT_ANGLE 0
#define ARM_TOP_ANGLE 180
#define ARM_DELAY 90
#define ARM_DELTA_DEGREE 15
#define ARM_SPEED 300

#define DYNAMIXEL_GOAL_POSITION 0x1e //DO NOT TOUCH
#define DYNAMIXEL_SPEED  0x20 //DO NOT TOUCH

#define MIN_MOTOR_DUTY 9
#define OFFSET_RAKE 6
#define DELAY_BACKDOOR 10
#define UART_BUFFER_SIZE 256

//#define MAX_DUTY 20.

void moveForward(double);
void moveBackward(double);
void turnRight(double);
void turnLeft(double);
void stopMoving();
void customMotorControl(int,int,double,double);
void setServoAngle(double, int);
//void setRakeAngle(double, int);
//void setBucketAngle(double, int);
void setBackdoorAngle(double angle, int delay);
int motor_command(UART_HandleTypeDef* huart, const char*);
int timed_motor_command(UART_HandleTypeDef* huart,  TIM_HandleTypeDef* htim , const char*);
//void rake_command(const char*);
//void bucket_command(const char*);
int open_backdoor_command(UART_HandleTypeDef* huart, const char* msg);
int set_arm_angle_command(UART_HandleTypeDef* huart, UART_HandleTypeDef* huart_dynamixel, const char* UART_in_buffer);
int gather_command(UART_HandleTypeDef* huart, UART_HandleTypeDef* huart_dynamixel, const char*  msg);
const char* remove_header(const char*);
void HCSR04_Read (TIM_HandleTypeDef* htim, uint32_t* sensors_data);
void US_callback(TIM_HandleTypeDef* htim, uint32_t* sensors_data);
void IR_Read(ADC_HandleTypeDef* hadc, uint32_t* adc_data);
void gatherBottle(UART_HandleTypeDef* huart, int delay);
//void initRakeAngle();
//void initBucketAngle();
void initBackdoorAngle();
void sendDynamixelCommand(UART_HandleTypeDef* huart, uint8_t dynamixel_id, uint8_t n_params, uint8_t* params);
void setDynamixelAngle(UART_HandleTypeDef* huart, uint8_t dynamixel_id, double angle);
int getDynamixelReturnStatus(UART_HandleTypeDef* huart);
void sendDynamixelCommandWithCheck(UART_HandleTypeDef* huart, uint8_t dynamixel_id, uint8_t n_params, uint8_t* params);
void dynamixelInit(UART_HandleTypeDef* huart);
void dynamixelReleaseTorque(UART_HandleTypeDef* huart);
#endif /* INC_DEVICES_H_ */
