/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

void delay_us (uint16_t us);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define IF5_Pin GPIO_PIN_0
#define IF5_GPIO_Port GPIOC
#define IF6_Pin GPIO_PIN_1
#define IF6_GPIO_Port GPIOC
#define IF7_Pin GPIO_PIN_2
#define IF7_GPIO_Port GPIOC
#define US_trigger1_Pin GPIO_PIN_3
#define US_trigger1_GPIO_Port GPIOC
#define IF1_Pin GPIO_PIN_0
#define IF1_GPIO_Port GPIOA
#define IF2_Pin GPIO_PIN_1
#define IF2_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define IF3_Pin GPIO_PIN_4
#define IF3_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define US_trigger2_Pin GPIO_PIN_4
#define US_trigger2_GPIO_Port GPIOC
#define IF4_Pin GPIO_PIN_1
#define IF4_GPIO_Port GPIOB
#define right_enable_Pin GPIO_PIN_10
#define right_enable_GPIO_Port GPIOB
#define left_enable_Pin GPIO_PIN_7
#define left_enable_GPIO_Port GPIOC
#define PWM_servo4_Pin GPIO_PIN_9
#define PWM_servo4_GPIO_Port GPIOC
#define right_PWM_speed_Pin GPIO_PIN_8
#define right_PWM_speed_GPIO_Port GPIOA
#define left_PWM_speed_Pin GPIO_PIN_9
#define left_PWM_speed_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define US_echo1_Pin GPIO_PIN_15
#define US_echo1_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define right_direction_Pin GPIO_PIN_4
#define right_direction_GPIO_Port GPIOB
#define left_direction_Pin GPIO_PIN_6
#define left_direction_GPIO_Port GPIOB
#define US_echo2_Pin GPIO_PIN_9
#define US_echo2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
