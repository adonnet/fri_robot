/*
 * behaviour.h
 *
 *  Created on: 8 juin 2022
 *      Author: Alain
 */

#ifndef INC_BEHAVIOUR_H_
#define INC_BEHAVIOUR_H_

#include "devices.h"
#include <stdbool.h>


#define N_FRONT_IR_SENSORS 3
// IR : 1000 - 30 cm ; 1500 - 20 cm
#define IR_THRESHOLD 2500 //NEAR ~30cm
#define US_THRESHOLD 2500
#define US_BOTTLE_THRESHOLD 900
#define US_VALID_LOW_THRESHOLD 300
#define US_VALID_THRESHOLD 23300
#define FAR_IR_THRESHOLD 700
//#define DIFF_THRESHOLD 100
#define SHOOTING_DIST 3
#define DIFF_US_RANGE_THRESHOLD 400
#define MAX_IR 10000
#define MAX_US 10000

#define MANEUVER_SPEED 16. //IN DUTY
#define GATHERING_SPEED 13.
#define WIGGLE_SPEED 14
#define WIGGLE_AMP 20
#define AVOID_DISTANCE 2. //IN CM
#define AVOID_ANGLE M_PI/6
#define END_AVOID_DISTANCE 10

#define END_OF_LOOP_VALUE 5000

#define INTERAVOIDANCE_DELAY 2500

void object_detection(uint32_t* range_data, bool* object_detected);
bool is_object_bottle(bool* object_detected);
bool object_in_front(bool* object_detected);
void decision_making_based_on_range_sensors(uint32_t* range_data, UART_HandleTypeDef* huart_dynamixel);
void obstacle_avoidance(uint32_t* range_data, int smallest_ir_id);
void gathering_protocol(UART_HandleTypeDef* huart_dynamixel, uint32_t smallest_us);
int DSpeedToDelay(double duty_speed, double distance);
void bottle_in_front(uint32_t* range_data, UART_HandleTypeDef* huart_dynamixel, uint32_t smallest_us);
void robotWiggle(int delay);


#endif /* INC_BEHAVIOUR_H_ */
