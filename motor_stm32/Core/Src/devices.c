#include "devices.h"
#include "behaviour.h"
#include <string.h>


static double i_backdoor_angle = 0.;

static uint32_t IC_Val1[N_US_SENSORS] = {0};
static uint32_t IC_Val2[N_US_SENSORS] = {0};
static uint32_t Difference[N_US_SENSORS] = {0};
static uint8_t Is_First_Captured[N_US_SENSORS] = {0};  // is the first value captured ?
static uint8_t US_value_captured[N_US_SENSORS] = {0};

int sign(int);

/**
 * Function to move forward
 */
void moveForward(double duty)
{
	//Activate the right motor (channel 1)
	HAL_GPIO_WritePin(right_direction_GPIO_Port, right_direction_Pin, R_FW_ROT);
	TIM1->CCR1 = (double) floor(duty/100.*TIM1->ARR);
	HAL_GPIO_WritePin(right_enable_GPIO_Port, right_enable_Pin, GPIO_PIN_SET);


	//Activate the left motor (channel 2)
	HAL_GPIO_WritePin(left_direction_GPIO_Port, left_direction_Pin, L_FW_ROT);
	TIM1->CCR2 = (double) floor(duty/100.*TIM1->ARR);
	HAL_GPIO_WritePin(left_enable_GPIO_Port, left_enable_Pin, GPIO_PIN_SET);
}

/**
 * Function to move backward
 */
void moveBackward(double duty)
{
	//Activate the right motor (channel 1)
	HAL_GPIO_WritePin(right_direction_GPIO_Port, right_direction_Pin, R_BW_ROT);
	TIM1->CCR1 = (double) floor(duty/100.*TIM1->ARR);
	HAL_GPIO_WritePin(right_enable_GPIO_Port, right_enable_Pin, GPIO_PIN_SET);


	//Activate the left motor (channel 2)
	HAL_GPIO_WritePin(left_direction_GPIO_Port, left_direction_Pin, L_BW_ROT);
	TIM1->CCR2 = (double) floor(duty/100.*TIM1->ARR);
	HAL_GPIO_WritePin(left_enable_GPIO_Port, left_enable_Pin, GPIO_PIN_SET);
}

/**
 * Function to turn right (wheel at opposite direction)
 */
void turnRight(double duty)
{
	//Activate the right motor (channel 1)
	HAL_GPIO_WritePin(right_direction_GPIO_Port, right_direction_Pin, R_BW_ROT);
	TIM1->CCR1 = (double) floor(duty/100.*TIM1->ARR);
	HAL_GPIO_WritePin(right_enable_GPIO_Port, right_enable_Pin, GPIO_PIN_SET);


	//Activate the left motor (channel 2)
	HAL_GPIO_WritePin(left_direction_GPIO_Port, left_direction_Pin, L_FW_ROT);
	TIM1->CCR2 = (double) floor(duty/100.*TIM1->ARR);
	HAL_GPIO_WritePin(left_enable_GPIO_Port, left_enable_Pin, GPIO_PIN_SET);
}

/**
 * Function to turn left (wheel at opposite direction)
 */
void turnLeft(double duty)
{
	//Activate the right motor (channel 1)
	HAL_GPIO_WritePin(right_direction_GPIO_Port, right_direction_Pin, R_FW_ROT);
	TIM1->CCR1 = (double) floor(duty/100.*TIM1->ARR);
	HAL_GPIO_WritePin(right_enable_GPIO_Port, right_enable_Pin, GPIO_PIN_SET);


	//Activate the left motor (channel 2)
	HAL_GPIO_WritePin(left_direction_GPIO_Port, left_direction_Pin, L_BW_ROT);
	TIM1->CCR2 = (double) floor(duty/100.*TIM1->ARR);
	HAL_GPIO_WritePin(left_enable_GPIO_Port, left_enable_Pin, GPIO_PIN_SET);
}


/**
 * Function to stop the robot
 */
void stopMoving()
{
	HAL_GPIO_WritePin(left_enable_GPIO_Port, left_enable_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(right_enable_GPIO_Port, right_enable_Pin, GPIO_PIN_RESET);
}

/**
 * Function for custom motor control
 */
void customMotorControl(int LDirection, int RDirection, double LDuty, double RDuty)
{
	//Activate the left motor (channel 2)
	HAL_GPIO_WritePin(left_direction_GPIO_Port, left_direction_Pin, LDirection);
	TIM1->CCR2 = (double) floor(LDuty/100.*TIM1->ARR);

	//Activate the right motor (channel 1)
	HAL_GPIO_WritePin(right_direction_GPIO_Port, right_direction_Pin, RDirection);
	TIM1->CCR1 = (double) floor(RDuty/100.*TIM1->ARR);

	if (LDuty >= MIN_MOTOR_DUTY)
		HAL_GPIO_WritePin(left_enable_GPIO_Port, left_enable_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(left_enable_GPIO_Port, left_enable_Pin, GPIO_PIN_RESET);


	if (RDuty >= MIN_MOTOR_DUTY)
		HAL_GPIO_WritePin(right_enable_GPIO_Port, right_enable_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(right_enable_GPIO_Port, right_enable_Pin, GPIO_PIN_RESET);

}


/**
 * Function to set the angle of a given servomotor
 * @param 	angle: angle in degree
 * 			servoNumber: number of the servo to control
 */
void setServoAngle(double angle, int servoNumber)
{
	double iAngle, maxAngle;

	if (servoNumber == 4)
		maxAngle = 180.;
	else
		maxAngle = 180.;


	if (angle > maxAngle)
		iAngle = maxAngle;
	else if (angle<0.)
		iAngle = 0.;
	else
		iAngle = angle;

	int CCR;
	if (servoNumber == 4)
		CCR = (double) floor((iAngle/maxAngle*2000. + 500.) / 20000. * TIM3->ARR);
	else
		CCR = (double) floor((iAngle/maxAngle*2000. + 500.) / 20000. * TIM3->ARR);

	switch (servoNumber)
	{
		case 1:
			TIM3->CCR1 = CCR;
			break;
		case 2:
			TIM3->CCR2 = CCR;
			break;
		case 3:
			TIM3->CCR3 = CCR;
			break;
		case 4:
			TIM3->CCR4 = CCR;
			break;
		default:
			//Do nothing, wrong servo number
			break;
	}
}


///**
// * Function to set the angle of the rake (servo2 as reference), step by 1 degree per delay
// * @param 	angle: angle in degree according to servo2
// */
//void setRakeAngle(double angle, int delay)
//{
//	int step = sign(angle-i_rake_angle);
//
//	while (fabs(angle-i_rake_angle) > 1e-9 )
//	{
//		i_rake_angle = (double)i_rake_angle + step;
//		setServoAngle(i_rake_angle + OFFSET_RAKE,2);
//		setServoAngle(270.-i_rake_angle,3);
//		HAL_Delay(delay);
//	}
//}
//
///**
// * Function to set the angle of the bucket, step by 1 degree per delay
// * @param 	angle: angle in degree
// */
//void setBucketAngle(double angle, int delay)
//{
//	int step = sign(angle-i_bucket_angle);
//
//	while (fabs(angle-i_bucket_angle) > 1e-9 )
//	{
//		i_bucket_angle = (double)i_bucket_angle + step;
//		setServoAngle(i_bucket_angle,1);
//		HAL_Delay(delay);
//	}
//}

void setBackdoorAngle(double angle, int delay)
{
	int step = sign(angle-i_backdoor_angle);

	while (fabs(angle-i_backdoor_angle) > 1e-9)
	{
		i_backdoor_angle = (double) i_backdoor_angle + step;
		setServoAngle(i_backdoor_angle,4);
		HAL_Delay(delay);
	}
}

int sign(int x)
{
	return (x > 0) - (x < 0);
}

int motor_command(UART_HandleTypeDef* huart, const char* msg)
{
	const char* p_msg = remove_header(msg);
	char* pEnd;

	double left_speed, right_speed;
	left_speed = strtod (p_msg, &pEnd);
	right_speed = strtod (pEnd, &pEnd);


	unsigned long int received_checksum = strtoul(pEnd,NULL, 10);

	unsigned long int measured_checksum = 0;
	p_msg = msg;
	while (p_msg < pEnd)
	{
		measured_checksum += (unsigned long int) *p_msg;
		p_msg ++;
	}


	  uint8_t UART_OK[] = "OK\n";
	  uint8_t UART_Failed[] = "Failed\n";
	if (measured_checksum != received_checksum)
	{
		  HAL_UART_Transmit_DMA(huart, UART_Failed, sizeof(UART_Failed));
		return 0;
	}
	else
	{
		HAL_UART_Transmit_DMA(huart, UART_OK, sizeof(UART_OK));
	}


	int L_direction;
	int R_direction;

	if (left_speed<0)
		L_direction = L_BW_ROT;
	else
		L_direction = L_FW_ROT;

	if (right_speed<0)
		R_direction = R_BW_ROT;
	else
		R_direction = R_FW_ROT;

	customMotorControl(L_direction,R_direction,fabs(left_speed),fabs(right_speed));

	return 1;
}

int timed_motor_command(UART_HandleTypeDef* huart, TIM_HandleTypeDef* htim , const char* msg)
{
	const char* p_msg = remove_header(msg);

	//extract double
	char* pEnd;
	double left_speed, right_speed, time;
	left_speed = strtod (p_msg, &pEnd);
	right_speed = strtod (pEnd, &pEnd);
	time = strtod(pEnd, &pEnd);


	unsigned long int received_checksum = strtoul(pEnd,NULL, 10);

	unsigned long int measured_checksum = 0;
	p_msg = msg;
	while (p_msg < pEnd)
	{
		measured_checksum += (unsigned long int) *p_msg;
		p_msg ++;
	}


	  uint8_t UART_OK[] = "OK\n";
	  uint8_t UART_Failed[] = "Failed\n";

	if (measured_checksum != received_checksum)
	{
		  HAL_UART_Transmit_DMA(huart, UART_Failed, sizeof(UART_Failed));
		return 0;
	}
	else
	{
		HAL_UART_Transmit_DMA(huart, UART_OK, sizeof(UART_OK));
	}


	int L_direction;
	int R_direction;

	if (left_speed<0)
		L_direction = L_BW_ROT;
	else
		L_direction = L_FW_ROT;

	if (right_speed<0)
		R_direction = R_BW_ROT;
	else
		R_direction = R_FW_ROT;

	customMotorControl(L_direction,R_direction,fabs(left_speed),fabs(right_speed));

	TIM5->ARR = time*2;
	TIM5->CNT = 0;
	__HAL_TIM_CLEAR_FLAG(htim, TIM_FLAG_UPDATE);
	HAL_TIM_Base_Start_IT(htim);

	return 1;

}

//// Rake_command
//// Angle given according servo2
//// Angle 0 : the arm is fully folded (bottom)
//// Angle 230 : ideal top position of the arm
//void rake_command(const char* msg)
//{
//	char* p_msg = remove_header(msg);
//
//	//extract double
//	char* pEnd;
//	double angle;
//	int delay;
//	angle = strtod (p_msg, &pEnd);
//	delay = (int) strtod (pEnd, NULL);
//
//	setRakeAngle(angle,delay);
//}
//
//// Bucket_command
//// Angle given according servo2
//// Angle 0 : the arm is fully folded (bottom)
//// Angle 230 : ideal top position of the arm
//void bucket_command(const char* msg)
//{
//	char* p_msg = remove_header(msg);
//
//	//extract double
//	char* pEnd;
//	double angle;
//	int delay;
//	angle = strtod (p_msg, &pEnd);
//	delay = (int) strtod (pEnd, NULL);
//
//	setBucketAngle(angle,delay);
//}

int open_backdoor_command(UART_HandleTypeDef* huart, const char* msg)
{
	const char* p_msg = remove_header(msg);
	char* pEnd;

	double duration;
	duration = strtod(p_msg,&pEnd);

	unsigned long int received_checksum = strtoul(pEnd,NULL, 10);

	unsigned long int measured_checksum = 0;
	p_msg = msg;
	while (p_msg < pEnd)
	{
		measured_checksum += (unsigned long int) *p_msg;
		p_msg ++;
	}


	  uint8_t UART_OK[] = "OK\n";
	  uint8_t UART_Failed[] = "Failed\n";

	if (measured_checksum != received_checksum)
	{
		  HAL_UART_Transmit_DMA(huart, UART_Failed, sizeof(UART_Failed));
		return 0;
	}
	else
	{
		HAL_UART_Transmit_DMA(huart, UART_OK, sizeof(UART_OK));
	}


	setBackdoorAngle(90, DELAY_BACKDOOR);
	robotWiggle((int) duration);
	setBackdoorAngle(0, DELAY_BACKDOOR);

	return 1;
}


int set_arm_angle_command(UART_HandleTypeDef* huart, UART_HandleTypeDef* huart_dynamixel, const char* msg)
{
	const char* p_msg = remove_header(msg);
	char* pEnd;

	double	angle = strtod(p_msg, &pEnd);
	unsigned long int received_checksum = strtoul(pEnd,NULL, 10);

	unsigned long int measured_checksum = 0;
	p_msg = msg;
	while (p_msg < pEnd)
	{
		measured_checksum += (unsigned long int) *p_msg;
		p_msg ++;
	}


	  uint8_t UART_OK[] = "OK\n";
	  uint8_t UART_Failed[] = "Failed\n";

	if (measured_checksum != received_checksum)
	{
		  HAL_UART_Transmit_DMA(huart, UART_Failed, sizeof(UART_Failed));
		return 0;
	}
	else
	{
		HAL_UART_Transmit_DMA(huart, UART_OK, sizeof(UART_OK));
	}

	int try_count = 0;

	do{
		try_count++;
		setDynamixelAngle(huart_dynamixel, DYNAMIXEL_ARM_ID, angle);
	} while (getDynamixelReturnStatus(huart_dynamixel) != 1 && try_count < MAX_TRY_UART3);

	return 1;
}

int gather_command(UART_HandleTypeDef* huart, UART_HandleTypeDef* huart_dynamixel, const char*  msg)
{
	const char* p_msg = remove_header(msg);
	char* pEnd;

	//extract double
	int delay;
	delay = (int) strtod (p_msg, &pEnd);
	unsigned long int received_checksum = strtoul(pEnd,NULL, 10);

	unsigned long int measured_checksum = 0;
	p_msg = msg;
	while (p_msg < pEnd)
	{
		measured_checksum += (unsigned long int) *p_msg;
		p_msg ++;
	}


	  uint8_t UART_OK[] = "OK\n";
	  uint8_t UART_Failed[] = "Failed\n";

	if (measured_checksum != received_checksum)
	{
		  HAL_UART_Transmit_DMA(huart, UART_Failed, sizeof(UART_Failed));
		return 0;
	}
	else
	{
		HAL_UART_Transmit_DMA(huart, UART_OK, sizeof(UART_OK));
	}


	gatherBottle(huart_dynamixel, delay);

	return 1;
}


const char* remove_header(const char* msg)
{
	const char* p_msg = (char *) msg;
	while (!isdigit((int) *p_msg) && *p_msg !='-') p_msg++;
	return p_msg;
}

void HCSR04_Read (TIM_HandleTypeDef* htim, uint32_t* sensors_data)
{
	static int i = 0;

	switch (i)
	{
	case 0:
		if (US_value_captured[N_US_SENSORS-1] == 0)
		{
			sensors_data[N_IR_SENSORS+N_US_SENSORS-1] = 60000;
		}

		__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_IT_CC1, TIM_INPUTCHANNELPOLARITY_RISING);
		HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_2);
		HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_1);

		HAL_GPIO_WritePin(US_trigger1_GPIO_Port, US_trigger1_Pin, GPIO_PIN_RESET);  // pull the TRIG pin low
		delay_us(2);
		HAL_GPIO_WritePin(US_trigger1_GPIO_Port, US_trigger1_Pin, GPIO_PIN_SET);  // pull the TRIG pin HIGH
		delay_us(10);  // wait for 10 us
		HAL_GPIO_WritePin(US_trigger1_GPIO_Port, US_trigger1_Pin, GPIO_PIN_RESET);  // pull the TRIG pin low
		US_value_captured[0] = 0;
		i++;
		break;
	case 1:
		if (US_value_captured[i-1] == 0)
		{
			sensors_data[N_IR_SENSORS+i-1] = 60000;
		}

		__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_IT_CC2, TIM_INPUTCHANNELPOLARITY_RISING);
		HAL_TIM_IC_Stop_IT(htim, TIM_CHANNEL_1);
		HAL_TIM_IC_Start_IT(htim, TIM_CHANNEL_2);

		HAL_GPIO_WritePin(US_trigger2_GPIO_Port, US_trigger2_Pin, GPIO_PIN_RESET);  // pull the TRIG pin low
		delay_us(2);
		HAL_GPIO_WritePin(US_trigger2_GPIO_Port, US_trigger2_Pin, GPIO_PIN_SET);  // pull the TRIG pin HIGH
		delay_us(10);  // wait for 10 us
		HAL_GPIO_WritePin(US_trigger2_GPIO_Port, US_trigger2_Pin, GPIO_PIN_RESET);  // pull the TRIG pin low
		US_value_captured[1]=0;
		i=0;
		break;
	default:
		i=0;
		break;

	}
}

void US_callback(TIM_HandleTypeDef* htim, uint32_t* sensors_data)
{
	if (htim->Instance == TIM2)
	{
		if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)  // if the interrupt source is channel1
		{
			if (Is_First_Captured[0]==0) // if the first value is not captured
			{
				IC_Val1[0] = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1); // read the first value
				Is_First_Captured[0] = 1;  // set the first captured as true
				// Now change the polarity to falling edge
				__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_1, TIM_INPUTCHANNELPOLARITY_FALLING);
			}

			else if (Is_First_Captured[0]==1)   // if the first is already captured
			{
				IC_Val2[0] = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);  // read second value
				__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter

				if (IC_Val2[0] > IC_Val1[0])
				{
					Difference[0] = IC_Val2[0]-IC_Val1[0];
				}

				else if (IC_Val1[0] > IC_Val2[0])
				{
					Difference[0] = (0xffff - IC_Val1[0]) + IC_Val2[0];
				}

				Is_First_Captured[0] = 0; // set it back to false
				sensors_data[N_IR_SENSORS] = Difference[0];
				US_value_captured[0] = 1;

				// set polarity to rising edge
				__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_1, TIM_INPUTCHANNELPOLARITY_RISING);
				__HAL_TIM_DISABLE_IT(htim, TIM_IT_CC1);
			}
		}
		if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)  // if the interrupt source is channel1
			{
				if (Is_First_Captured[1]==0) // if the first value is not captured
				{
					IC_Val1[1] = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2); // read the first value
					Is_First_Captured[1] = 1;  // set the first captured as true
					// Now change the polarity to falling edge
					__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_2, TIM_INPUTCHANNELPOLARITY_FALLING);
				}

				else if (Is_First_Captured[1]==1)   // if the first is already captured
				{
					IC_Val2[1] = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);  // read second value
					__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter

					if (IC_Val2[1] > IC_Val1[1])
					{
						Difference[1] = IC_Val2[1]-IC_Val1[1];
					}

					else if (IC_Val1[1] > IC_Val2[1])
					{
						Difference[1] = (0xffff - IC_Val1[1]) + IC_Val2[1];
					}

					Is_First_Captured[1] = 0; // set it back to false
					sensors_data[N_IR_SENSORS+1] = Difference[1];
					US_value_captured[1] = 1;

					// set polarity to rising edge
					__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_2, TIM_INPUTCHANNELPOLARITY_RISING);
					__HAL_TIM_DISABLE_IT(htim, TIM_IT_CC2);
				}
			}
	}
}

void IR_Read(ADC_HandleTypeDef* hadc, uint32_t* adc_data)
{
	for (int i=0; i<N_IR_SENSORS/2; i++)
	{
		  adc_data[i]=0;

	}

		HAL_ADC_Start_DMA(hadc, adc_data, N_IR_SENSORS); //55ms
}

//void gatherBottle(int delay)
//{
//		setRakeAngle(150,delay);
//		setBucketAngle(230,delay);
//		setRakeAngle(30,delay);
//		setRakeAngle(130,delay);
//		setBucketAngle(0, delay);
//
//		//init position, fixed delay to avoid unwanted behaviour
//		setBucketAngle(270, 10);
//		setRakeAngle(230,10);
//}

void gatherBottle(UART_HandleTypeDef* huart, int delay)
{
	setDynamixelAngle(huart, DYNAMIXEL_ARM_ID, ARM_BOT_ANGLE);
	HAL_Delay(delay);
	setDynamixelAngle(huart, DYNAMIXEL_ARM_ID, ARM_TOP_ANGLE);

	  HAL_Delay(1500);
	  dynamixelReleaseTorque(huart);
}

//void initRakeAngle()
//{
//	setServoAngle(i_rake_angle+OFFSET_RAKE,2);
//	setServoAngle(270.-i_rake_angle,3);
//}
//
//void initBucketAngle()
//{
//	setServoAngle(i_bucket_angle,1);
//}

void initBackdoorAngle()
{
	setServoAngle(i_backdoor_angle,4);
}

void setDynamixelAngle(UART_HandleTypeDef* huart, uint8_t dynamixel_id, double angle)
{
	double i_angle;
	if (angle<0)
	{
		i_angle = 0;
	}
	else if (angle>300)
	{
		i_angle = 300;
	}
	else
	{
		i_angle = angle;
	}

	uint16_t byte = (double) i_angle/300 * 0x3ff;

	uint8_t params[3];
	params[0] = DYNAMIXEL_GOAL_POSITION;
	params[1] = byte & 0xff;
	params[2] = (byte >> 8);

	sendDynamixelCommandWithCheck(huart, dynamixel_id, 3, params);
}

void sendDynamixelCommand(UART_HandleTypeDef* huart, uint8_t dynamixel_id, uint8_t n_params, uint8_t* params)
{
	const uint8_t header = 0xff;
	const uint8_t length = n_params + 2;
	const uint8_t instruction = 0x03;
	uint8_t checksum = dynamixel_id + length + instruction;

	uint32_t timeout = UART3_TIMEOUT;

	HAL_HalfDuplex_EnableTransmitter(huart);
	HAL_UART_Transmit(huart, &header, sizeof(header), timeout);
	HAL_UART_Transmit(huart, &header, sizeof(header), timeout);
	HAL_UART_Transmit(huart, &dynamixel_id, sizeof(dynamixel_id), timeout);
	HAL_UART_Transmit(huart, &length, sizeof(length), timeout);
	HAL_UART_Transmit(huart, &instruction, sizeof(instruction), timeout);

	for (int i=0; i<n_params; i++)
	{
		HAL_UART_Transmit(huart, params+i, sizeof(*params), timeout);
		checksum += params[i];
	}

	checksum = ~checksum;
	HAL_UART_Transmit(huart, &checksum, sizeof(checksum), timeout);
}

//return 1 if the command succeed, 0 otherwise
int getDynamixelReturnStatus(UART_HandleTypeDef* huart)
{
	uint8_t temp_buffer[6] = {0};
	uint8_t checksum = 0;
	HAL_HalfDuplex_EnableReceiver(huart);
	HAL_UART_Receive(huart, temp_buffer, 6, UART3_TIMEOUT);
	for (int i=0; i<3; i++)
	{
		checksum += temp_buffer[2+i];
	}
	checksum = ~checksum;
	if (temp_buffer[4] != 0 || temp_buffer[5] !=  checksum) //CHECK ERROR
	{
	  return 0;
	}
	return 1;
}

void sendDynamixelCommandWithCheck(UART_HandleTypeDef* huart, uint8_t dynamixel_id, uint8_t n_params, uint8_t* params)
{
	int try_count = 0;

	do{
		try_count++;
		sendDynamixelCommand(huart, dynamixel_id, n_params, params);
	} while (getDynamixelReturnStatus(huart) != 1 && try_count < MAX_TRY_UART3);
}

void dynamixelInit(UART_HandleTypeDef* huart)
{
	uint8_t speed_params[3] = {DYNAMIXEL_SPEED, ARM_SPEED&0xff, ARM_SPEED>>8};
	sendDynamixelCommandWithCheck(huart, DYNAMIXEL_ARM_ID, 3, speed_params);
	HAL_Delay(1000);
	uint8_t torque_enable_params[2] = {24,0}; //release torque
	sendDynamixelCommandWithCheck(huart, DYNAMIXEL_ARM_ID,2, torque_enable_params);
}

void dynamixelReleaseTorque(UART_HandleTypeDef* huart)
{
	  uint8_t torque_enable_params[2] = {24,0}; //release torque
	  sendDynamixelCommandWithCheck(huart, DYNAMIXEL_ARM_ID,2, torque_enable_params);
}
