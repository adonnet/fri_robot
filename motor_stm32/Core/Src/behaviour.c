#include "behaviour.h"
#include <math.h>

#define MAX_IR 10000
#define MAX_US 10000

void object_detection(uint32_t* range_data, bool* object_detected)
{
	for (int i=0; i<N_IR_SENSORS; i++)
	{
		if (range_data[i]>IR_THRESHOLD)
		{
			object_detected[i] = true;
		}
	}
	for (int i=N_IR_SENSORS; i<N_IR_SENSORS+N_US_SENSORS; i++)
	{
		if (range_data[i]<US_THRESHOLD && !(range_data[i]>US_VALID_THRESHOLD))
		{
			object_detected[i] = true;
		}
	}
}

bool object_in_front(bool* object_detected)
{
	bool object_in_front = false;
	for (int i=0; i<N_FRONT_IR_SENSORS; i++)
	{
		object_in_front = object_in_front || object_detected[i];
	}
	for (int i=N_IR_SENSORS; i< N_IR_SENSORS+N_US_SENSORS; i++)
	{
		object_in_front = object_in_front || object_detected[i];
	}
	return object_in_front;
}

void decision_making_based_on_range_sensors(uint32_t* range_data, UART_HandleTypeDef* huart_dynamixel)
{
	for (int i=0; i<N_FRONT_IR_SENSORS; i++)
	{
		if (range_data[i] > IR_THRESHOLD)
		{
			obstacle_avoidance(range_data, i);
			return;
		}
	}

	for (int i=N_IR_SENSORS; i<N_IR_SENSORS+N_US_SENSORS; i++) //for each ultrasound sensors
	{
		if (range_data[i]<US_THRESHOLD && !(range_data[i]>US_VALID_THRESHOLD) && !(range_data[i]<US_VALID_LOW_THRESHOLD))
		{
			for (int j=0; j<N_FRONT_IR_SENSORS; j++)
			{
				if (range_data[j]>FAR_IR_THRESHOLD)
				{
					return;
				}
			}

			bottle_in_front(range_data, huart_dynamixel, range_data[i]);
			return;
		}
	}
}


void obstacle_avoidance(uint32_t* range_data, int smallest_ir_id)
{
	int delay;
	int id;
	uint32_t time = HAL_GetTick();


	static uint32_t old_time = 0;
	static int old_id = -1;


	if (time - old_time < INTERAVOIDANCE_DELAY)
	{
		id=old_id;
	}
	else
	{
		id=smallest_ir_id;
		old_id = smallest_ir_id;
	}

	old_time = time;

	switch (id)
	{

	// obstacle in left, go right
	case 0:
		customMotorControl(L_BW_ROT, R_BW_ROT, MANEUVER_SPEED, 10);
		delay = DSpeedToDelay(MANEUVER_SPEED, AVOID_ANGLE * 22.);
		HAL_Delay(delay);
		moveForward(MANEUVER_SPEED);
		delay = DSpeedToDelay(MANEUVER_SPEED, AVOID_DISTANCE);
		HAL_Delay(delay);
		moveForward(10);
		break;
	// obstacle in middle, go left
	case 1:
		customMotorControl(L_BW_ROT, R_BW_ROT, MANEUVER_SPEED, 10);
		delay = DSpeedToDelay(MANEUVER_SPEED, AVOID_ANGLE * 22.);
		HAL_Delay(delay);
		moveForward(MANEUVER_SPEED);
		delay = DSpeedToDelay(MANEUVER_SPEED, AVOID_DISTANCE);
		HAL_Delay(delay);
		moveForward(10);
		break;
	// obstacle in right, go left
	case 2:
		customMotorControl(L_BW_ROT, R_BW_ROT, 10, MANEUVER_SPEED);
		delay = DSpeedToDelay(MANEUVER_SPEED, AVOID_ANGLE * 22.);
		HAL_Delay(delay);
		moveForward(MANEUVER_SPEED);
		delay = DSpeedToDelay(MANEUVER_SPEED, AVOID_DISTANCE);
		HAL_Delay(delay);
		moveForward(10);
		break;
	default:
		moveForward(10);
		break;
	}
	int smallest_id = -1;
	int smallest = -1;
	for (int i=0; i<N_FRONT_IR_SENSORS; i++)
	{
		if ((range_data[i] > IR_THRESHOLD) && (range_data[i] > smallest))
		{
			smallest = range_data[i];
			smallest_id = i;
		}
	}
	if (smallest_id >= 0)
	{
		obstacle_avoidance(range_data, smallest_id);
	}
	delay = DSpeedToDelay(MANEUVER_SPEED, END_AVOID_DISTANCE);
	for (int j=0; j<delay; j=j+IR_DELAY)
	{
		moveForward(MANEUVER_SPEED);
		HAL_Delay(IR_DELAY);
		for (int k=0; k<N_FRONT_IR_SENSORS; k++)
		{
			if (range_data[k] > IR_THRESHOLD)
			{
				obstacle_avoidance(range_data, k);
				goto out_of_double_loop;
			}
		}
	}
	out_of_double_loop:
	moveForward(10); //MAINTAIN TORQUE
}

//smallest us is raw data from US (conversion to cm is made in the function)
void gathering_protocol(UART_HandleTypeDef* huart_dynamixel, uint32_t smallest_us)
{
	moveForward(GATHERING_SPEED);
	int delay = DSpeedToDelay(GATHERING_SPEED, SHOOTING_DIST + 10);
	HAL_Delay(delay);
	// move backward SHOOTING_DIST
	moveBackward(GATHERING_SPEED);
	delay = DSpeedToDelay(GATHERING_SPEED, SHOOTING_DIST);
	HAL_Delay(delay);
	moveForward(10);
	gatherBottle(huart_dynamixel, 2000);
	HAL_Delay(500);
}


int DSpeedToDelay(double duty_speed, double distance)
{
	int delay = 0;
	return delay = (double) distance /((duty_speed - 10.)/80. * 10000. * 2*M_PI / 60. / 18. * 4.)*1000.;
}


void bottle_in_front(uint32_t* range_data, UART_HandleTypeDef* huart_dynamixel, uint32_t smallest_us)
{
	uint32_t timeout_counter=0;
	while (((range_data[7] > US_BOTTLE_THRESHOLD || range_data[7]>US_VALID_THRESHOLD || range_data[7]<US_VALID_LOW_THRESHOLD) &&
			(range_data[8] > US_BOTTLE_THRESHOLD || range_data[8]>US_VALID_THRESHOLD|| range_data[8]<US_VALID_LOW_THRESHOLD)) && (timeout_counter < END_OF_LOOP_VALUE))
	{
		if (range_data[7]>US_VALID_LOW_THRESHOLD && range_data[8]>US_VALID_LOW_THRESHOLD)
		{
			if (range_data[7] < US_THRESHOLD && range_data[8] > US_THRESHOLD)
			{
				customMotorControl(L_FW_ROT,R_FW_ROT, GATHERING_SPEED, 10);
			}
			else if (range_data[7] > US_THRESHOLD && range_data[8] < US_THRESHOLD)
			{
				customMotorControl(L_FW_ROT,R_FW_ROT, 10, GATHERING_SPEED);
			}
			else
			{
				moveForward(GATHERING_SPEED);
			}
		}



		timeout_counter+=10;
		HAL_Delay(10);
	}
	moveForward(10);
	for (int j=0; j<N_FRONT_IR_SENSORS; j++)
	{
		if (range_data[j]>FAR_IR_THRESHOLD)
		{
			return;
		}
	}
	gathering_protocol(huart_dynamixel, 0);
}

void robotWiggle(int delay)
{
	for (int i=0; i<delay; i=i+500)
	{
		customMotorControl(L_BW_ROT, R_BW_ROT, WIGGLE_SPEED, WIGGLE_SPEED);
		HAL_Delay(250);
		customMotorControl(L_FW_ROT, R_FW_ROT, WIGGLE_SPEED, WIGGLE_SPEED);
		HAL_Delay(250);
	}
	moveForward(10);
}

