This is the full CAD of our robot "Fri-bot" that we built on the occasion of the 2022 Interdisciplinary Robotics Competition at EPFL.
The actual robot has very few tweaks in the real life in comparisons to this design: 
- the front ultrasound sensors were slightly lowered using two short MDF extension pieces to give them a better visibility of bottles
- the backdoor actuation was mounted on the other side (this does not change anything)
- some holes had to be drilled for mounting of certain elements
- two side bumbers made of pet bottle and a massive front bumper using MDF & foam to protect the front sensors in any unfortunate test failure
- two access ports were cut in the side panels to allow hands to reach more easily the cables connected on the Jetson/STM32
- one big removable access panel was cut in the slope along with 3 MDF wedges, making all maintenance extremely effective (a battery change for example would take less than 30 seconds)
- the acual picking grill was layered with duct tape stripes over foam stripes as per described in the report
- we had to file down a part of the structure because the wheels were rubbing against it