# README for the STI robot competition

## Goal of the project

The aim of this project was to design a solution to collect a maximum of points through PET bottles
gathering in a specific arena, in a ten minutes limit.

## Structure

+ README
+ ros_catkin_ws: contains all the written or modified files to run on the JETSON - ROS based
+ motor_stm32: contains all the script for the STM32
+ CAD: contains the robot's CAD, see CAD/README.txt for more details
