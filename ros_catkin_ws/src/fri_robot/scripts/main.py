#!/usr/bin/env python3
import rospy
import math
import time
from std_msgs.msg import Bool
from std_msgs.msg import Float64
from fri_robot.msg import MotorSpeed as motorSpeed
from fri_robot.msg import BottlePosition as bottlePosition
from fri_robot.srv import GetLocalisation, GetLocalisationResponse 

def us_callback(data):
    if (debug):
        rospy.loginfo("received us_message: %s", data)

def bottle_detected(data):
    FORWARD_DIST = 0.6
    dist = data.distance
    angle = data.angle
    if (verbose):
        rospy.loginfo("detected dist "+str(round(dist, 2))+ " and angle "+str(round(angle, 2)) )
    move(angle, FORWARD_DIST)

def speed_to_pwm(speed, MIN_SPEED, MAX_SPEED):
    if speed < MIN_SPEED:
        return 10
    elif speed > MAX_SPEED:
        return 90
    else:
        return 10 + speed / MAX_SPEED * 80

def manual_stop():
    if (debug):
        rospy.loginfo("========================> MANUAL STOP")
    motor_speed = motorSpeed()
    motor_speed.left_speed = 10 
    motor_speed.right_speed = 10 
    motor_speed.duration = 0.001 

    if motor_speed.duration > 0:
        motor_pub.publish(motor_speed)
        time.sleep(motor_speed.duration)

def turn(angle):
    if (abs(angle) > 0.1):
        # Checks if the opposite angle requires less turning
        opposite_angle = angle - math.pi*2*sign(angle)
        if(abs(opposite_angle) < abs(angle)):
            angle = opposite_angle

        AXLE_LENGTH = 0.22
        WHEEL_RADIUS = 0.04
        MIN_SPEED_MOTOR = 35
        MAX_SPEED_MOTOR = 10000/18
        MIN_SPEED = MIN_SPEED_MOTOR * 2 * math.pi / 60
        MAX_SPEED = MAX_SPEED_MOTOR * 2 * math.pi / 60
        FORWARD_SPEED = MIN_SPEED

        turn_time = abs(angle) * AXLE_LENGTH / MIN_SPEED / WHEEL_RADIUS / 2
        turn_time = turn_time * 0.93
        motor_left = 0
        motor_right = 0

        if angle > 0:
            motor_left = -speed_to_pwm(MIN_SPEED, MIN_SPEED, MAX_SPEED)
            motor_right = speed_to_pwm(MIN_SPEED, MIN_SPEED, MAX_SPEED)

        elif angle < 0:
            motor_left = speed_to_pwm(MIN_SPEED, MIN_SPEED, MAX_SPEED)
            motor_right = -speed_to_pwm(MIN_SPEED, MIN_SPEED, MAX_SPEED)

        elif angle == 0:
            motor_left = motor_right = 0

        motor_speed = motorSpeed()
        motor_speed.left_speed = motor_left
        motor_speed.right_speed = motor_right
        motor_speed.duration = turn_time

        if (debug):
            rospy.loginfo("========================> TURN : "+ str(round(turn_time, 2)))
        if turn_time > 0:
            motor_pub.publish(motor_speed)
        time.sleep(turn_time)
        if(TEMP):
            manual_stop()

def forward(distance):
    if (abs(distance) > 0.1):
        AXLE_LENGTH = 0.22
        WHEEL_RADIUS = 0.04
        MIN_SPEED_MOTOR = 35
        MAX_SPEED_MOTOR = 10000/18
        MIN_SPEED = MIN_SPEED_MOTOR * 2 * math.pi / 60
        MAX_SPEED = MAX_SPEED_MOTOR * 2 * math.pi / 60
        FORWARD_SPEED = MIN_SPEED*2.5

        if(distance < 0):
            FORWARD_SPEED = MIN_SPEED


        moving_time = abs(distance) /1.2 / WHEEL_RADIUS / FORWARD_SPEED
        motor_left = 0
        motor_right = 0

        motor_left = speed_to_pwm(FORWARD_SPEED, MIN_SPEED, MAX_SPEED)
        motor_right = speed_to_pwm(FORWARD_SPEED, MIN_SPEED, MAX_SPEED)

        motor_speed = motorSpeed()
        motor_speed.left_speed = motor_left
        motor_speed.right_speed = motor_right
        motor_speed.duration = moving_time 

        if(distance < 0):
            if (debug):
                rospy.loginfo("========================> MOVE BACKWARD: "+ str(round(moving_time, 2)))
            motor_speed.left_speed = -motor_speed.left_speed 
            motor_speed.right_speed = -motor_speed.right_speed 
        else:
            if (debug):
                rospy.loginfo("========================> MOVE FORWARD: "+ str(round(moving_time, 2)))
        motor_speed.duration = moving_time 
        if moving_time > 0:
            motor_pub.publish(motor_speed)
        time.sleep(moving_time)
        if(TEMP):
            manual_stop()

    return 0

def move(angle, distance):
    turn(angle)
    forward(distance)

def sign(x):
    if(x >= 0):
        return 1
    else:
        return -1
# given a target position and a current position and angle
# returns the needed angle and distance to move
def position_to_movement(tpx, tpy, cpx, cpy, ca):
    dy = tpy - cpy
    dx = tpx - cpx
    target_angle = math.atan2(dy,dx)
    angle = target_angle - ca
    
    distance = math.sqrt(dy*dy + dx*dx)
    if (verbose):
        rospy.loginfo("TARGET ANGLE="+str(round(target_angle, 2))+" ANGLE="+str(round(angle, 2))+" DIST="+str(round(distance, 2)))
    return angle, distance

def get_localisation():
    if (debug):
        rospy.loginfo("WAITING...")
    rospy.wait_for_service("compute_localisation")
    if (debug):
        rospy.loginfo("SERVICE OK")
    try:
        get_localisation = rospy.ServiceProxy('compute_localisation', GetLocalisation)
        loca = get_localisation()
        if (verbose):
            rospy.loginfo("GOT LOCALISATION =====>"+ str(round(loca.pos_x, 2)) + " | " + str(round(loca.pos_y, 2)) + " | " + str(round(loca.angle, 2)))
        return loca.pos_x, loca.pos_y, loca.angle
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

# Returns true if the current position is close enough to the target position
def goal_reached(tpx, tpy, cpx, cpy, dist_threshold):
    return ((abs(tpx-cpx) < dist_threshold) and 
            (abs(tpy-cpy) < dist_threshold))

def update_localisation(dist_threshold, counter):
    manual_stop()
    cpx, cpy, ca = get_localisation()
    if (cpx < 0 or cpy < 0):
        if (verbose):
            rospy.loginfo("GOT BAD LOCALISATION")
        if (dist_threshold < THRESHOLD and counter < 2):
            if (verbose):
                rospy.loginfo("TRYING AGAIN")
            return update_localisation(dist_threshold, counter+1)
        else:
            return cpx, cpy, ca
    else:
        return cpx, cpy, ca

def wander(dist_threshold):
    if (verbose):
        rospy.loginfo("===========> WANDERING")
    cpx = cpy = -1
    while (cpx < 0 or cpy < 0):
        forward(-dist_threshold)
        cpx, cpy, ca = update_localisation(dist_threshold, 1)
    if (verbose):
        rospy.loginfo("WANDERING FINISHED <============")
    return cpx, cpy, ca

def on_platform(px, py):
    MIN_X_PLAT = 0
    MAX_X_PLAT = 3
    MIN_Y_PLAT = 0
    MAX_Y_PLAT = 2
    return (px >= MIN_X_PLAT and px <= MAX_X_PLAT) and (py >= MIN_Y_PLAT and py <= MAX_Y_PLAT)

def go_to_check(tpx, tpy, ta, dist_threshold, time_limit, prev_wp):
    cpx, cpy, ca = update_localisation(dist_threshold, 0)
    robot_on_platform = on_platform(cpx, cpy)
    target_on_platform = on_platform(tpx, tpy)
    on_plat = (robot_on_platform, target_on_platform)
    wp = (tpx, tpy, ta, dist_threshold, time_limit)
    if (robot_on_platform and not target_on_platform): # Needs to go down
        way_points = down_slope_to
        go_through_wps(way_points, time_limit)
    elif (not robot_on_platform and target_on_platform): # Needs to go up
        way_points = up_slope_to
        go_through_wps(way_points, time_limit)
    else:
        if (ta is not None):
            go_to_angle(tpx, tpy, ta, dist_threshold, time_limit)
        else:
            go_to(tpx, tpy, dist_threshold, time_limit)

def go_to(tpx, tpy, dist_threshold, time_limit):
    MAX_DISTANCE = 0.5
    MAX_BIG_DISTANCE = 2
    MAX_PRECISE_DISTANCE = 0.3
    MAX_ANGLE = math.pi/2
    if(dist_threshold < THRESHOLD): # If we are doing precision work, use the precise max dist
        MAX_DISTANCE = MAX_PRECISE_DISTANCE
    elif(dist_threshold > THRESHOLD):
        MAX_DISTANCE = MAX_BIG_DISTANCE
        if (verbose):
            rospy.loginfo("||||| USING BIG DISTANCE "+ round_two_dec(MAX_BIG_DISTANCE)+" |||||")
    starting_time = time.time()

    cpx, cpy, ca = update_localisation(dist_threshold, 0)

    elapsed_time = time.time() - starting_time
    while (not goal_reached(tpx, tpy, cpx, cpy, dist_threshold) and
           elapsed_time < time_limit):
        if (verbose):
            rospy.loginfo("REMAINING TIME: "+ str(round(time_limit - elapsed_time, 2)))
        if (cpx < 0 or cpy < 0):
            cpx, cpy, ca = wander(dist_threshold)
        else:
            angle, distance = position_to_movement(tpx, tpy, cpx, cpy, ca)
            if (verbose):
                rospy.loginfo("TARGET LOCALISATION: "+ str(round(tpx, 2)) + " -- " + str(round(tpy, 2)))
                rospy.loginfo("COMPUTED MOVEMENT (angle-dist): "+ str(angle) + " -- " + str(distance))
            if (abs(angle) > MAX_ANGLE):
                angle = MAX_ANGLE*sign(angle)
                turn(angle)
            else:
                if (distance > MAX_DISTANCE):
                    distance = MAX_DISTANCE
                move(angle, distance)
            cpx, cpy, ca = update_localisation(dist_threshold, 0)
            elapsed_time = time.time() - starting_time
    if (verbose):
        if (goal_reached(tpx, tpy, cpx, cpy, dist_threshold)):
            rospy.loginfo("|||||||||||||| GOAL REACHED: "+round_two_dec(tpx)+";"+round_two_dec(tpy)+" |||||||||||||||||")
        elif (elapsed_time >= time_limit):
            rospy.loginfo("|||||||||||||| TIME's UP |||||||||||||||||")
    return cpx, cpy, ca

def wander_angle(dist_threshold):
    TURN_CONST = 0.05
    if (verbose):
        rospy.loginfo("===========> WANDERING FOR THE ANGLE")
    cpx = cpy = -1
    while (cpx < 0 or cpy < 0):
        turn(TURN_CONST)
        cpx, cpy, ca = update_localisation(dist_threshold, 1)
    if (verbose):
        rospy.loginfo("WANDERING FOR THE ANGLE FINISHED <============")
    return cpx, cpy, ca

def check_n_turn(ta, cpx, cpy, ca, dist_threshold):
    if (cpx < 0 or cpy < 0):
        return wander_angle(dist_threshold)
    else:
        needed_angle = ta - ca
        turn(needed_angle)
        cpx, cpy, ca = update_localisation(dist_threshold, 0)
        return cpx, cpy, ca


def go_to_angle(tpx, tpy, ta, dist_threshold, time_limit):
    cpx, cpy, ca = go_to(tpx, tpy, dist_threshold, time_limit)
    ANGLE_THRESHOLD = 0.1
    MAX_TRIES = 5
    nb_tries = 0
    while (abs(ta-ca) > ANGLE_THRESHOLD and nb_tries < MAX_TRIES):
        if (verbose):
            rospy.loginfo("current and target angles: "+ str(round(ca, 2)) + " -- " + str(round(ta, 2)))
        cpx, cpy, ca = check_n_turn(ta, cpx, cpy, ca, dist_threshold)
        nb_tries += 1
    if (verbose):
        if (abs(ta-ca) < ANGLE_THRESHOLD):
            rospy.loginfo("|||||||||||||| ANGLE REACHED: "+str(round(ta, 2))+"|||||||||||||||||")
        else:
            rospy.loginfo("|||||||||||||| TIME's UP ANGLE |||||||||||||||||")
    return cpx, cpy, ca

def compute_timeout(tpx, tpy, cpx, cpy):
    SEC_PER_METER = 15
    CONST_TURN_TIME = 15
    angle, distance = position_to_movement(tpx, tpy, cpx, cpy, 0)
    return distance*SEC_PER_METER + CONST_TURN_TIME

def add_timeout_to_target_points(way_points, prev_wp):
    target_points_with_to = []
    for wp in way_points:
        cpx = prev_wp[0]
        cpy = prev_wp[1]
        tpx = wp[0]
        tpy = wp[1]
        ta = wp[2]
        timeout = compute_timeout(tpx, tpy, cpx, cpy)
        precision_threshold = wp[3]
        new_wp = (tpx, tpy, ta, precision_threshold, timeout)
        target_points_with_to.append(new_wp)
        prev_wp = wp
    return target_points_with_to

def go_through_check(wps, time_limit, prev_wp):
    starting_time = time.time()
    TIME_LIMIT = time_limit
    elapsed_time = time.time() - starting_time
    for wp in wps:
        if(elapsed_time < TIME_LIMIT):
            if (verbose):
                rospy.loginfo("general time limit: " + str(round(TIME_LIMIT - elapsed_time, 2)))
            go_to_check(wp[0], wp[1], wp[2], wp[3], wp[4], prev_wp)
            prev_wp = wp

def go_through_wps(way_points, time_limit):
    starting_time = time.time()
    TIME_LIMIT = time_limit
    elapsed_time = time.time() - starting_time

    for wp in way_points:
        if(elapsed_time < TIME_LIMIT):
            if (verbose):
                rospy.loginfo("general time limit: " + str(round(TIME_LIMIT - elapsed_time, 2)))

            if(wp[2] is not None):
                go_to_angle(wp[0], wp[1], wp[2], wp[3], wp[4])
            else:
                go_to(wp[0], wp[1], wp[3], wp[4])
            elapsed_time = time.time() - starting_time

def try_n_get_one():
    DIST_TO_GO = 2
    HOME_TIME_LIMIT = 300
    forward(DIST_TO_GO)
    home_to = add_timeout_to_target_points(home, (DIST_TO_GO, 8))
    go_through_wps(home_to, HOME_TIME_LIMIT)
    drop_bottles()

def drop_bottles():
    DROPPING_TIME_MS = 2000.
    dropping_pub.publish(DROPPING_TIME_MS)
    if (verbose):
        rospy.loginfo("dropped bottles")

def round_two_dec(x):
    if (x is None):
        return x
    else:
        return "{:.2f}".format(x)

def print_wps(wps):
    wp_i = 0
    rospy.loginfo("+------+------+------+------+------+------+")
    for wp in wps:
        if (wp[2] is not None):
            angle = str(round_two_dec(wp[2]))
        else:
            angle = "None"
        rospy.loginfo("| wp_"+str(wp_i)+" | "+str(round_two_dec(wp[0]))+" | "
                      +str(round_two_dec(wp[1]))+" | "+angle+" | "
                      +str(round_two_dec(wp[3]))+" | "+str(round_two_dec(wp[4]))+" |")
        wp_i += 1
    rospy.loginfo("+------+------+------+------+------+------+")

if __name__ == '__main__':
    try:
        THRESHOLD = 0.5
        PRECISION_THRESHOLD = 0.25
        BIG_THRESHOLD = THRESHOLD + 0.01
        TIMEOUT = 50
        TEMP = True
        GENERAL_TIME_LIMIT = 10*60
        grass_time_limit = 5*60
        cross_time_limit = 3*60
        platform_time_limit = 5*60
        debug = False
        verbose = True
        home_pt = (7.25,7.25)
        full_strat = False 

        # points where the robot will go one after another
        # (x, y, Opt(angle), Opt(precision_threshold)), Opt(timeout)
        home = [(7, 7, None, THRESHOLD), (7.25,7.25, -3*math.pi/4, PRECISION_THRESHOLD)]
        home_to = add_timeout_to_target_points(home, (0,0))
        reach_slope = [(4, 4, None, BIG_THRESHOLD, 60)]
        platform = [(0.45, 0.45, 0, PRECISION_THRESHOLD),
                    (2.25, 0.5, 3*math.pi/4, PRECISION_THRESHOLD),
                    (1.5,1.5, -3*math.pi/4, PRECISION_THRESHOLD)]
        platform_to = add_timeout_to_target_points(platform, (0.35,3.25))
        up_slope_to = [(0.35, 3, None, THRESHOLD, 120),
                    (0.25, 2.5, -math.pi/2, PRECISION_THRESHOLD, 120),
                    (0.25, 0.5, None, BIG_THRESHOLD, 120)]
        down_slope = [(0.6, 0.6, -3*math.pi/4, PRECISION_THRESHOLD),
                      (0.15, 0.75, None, PRECISION_THRESHOLD),
                      (0.25, 2.5, None, THRESHOLD)] # in front of the slope
        down_slope_to = add_timeout_to_target_points(down_slope, (2.25,2.25))
        platform_full = reach_slope + up_slope_to + platform_to + down_slope_to

        grass = [(4.25, 7.25, None, THRESHOLD),
                (1.75, 7.25, None, THRESHOLD),
                (1, 5.25, None, THRESHOLD),
                (4.25, 5.25, None, THRESHOLD)]
        grass_to = add_timeout_to_target_points(grass, home_pt)
        cross = [(4, 4, None, THRESHOLD),
                (7, 4.5, None, THRESHOLD)]
        cross_to = add_timeout_to_target_points(cross, home_pt)

        dropping_pub = rospy.Publisher('dropping', Float64, queue_size=1)
        ask = True
        motor_pub = rospy.Publisher('motor_speed', motorSpeed, queue_size=0)

        rospy.init_node('main', anonymous=False)
        rate = rospy.Rate(10)

        #rospy.Subscriber("us_publisher", Bool, us_callback)
        rospy.Subscriber("bottle_position", bottlePosition, bottle_detected)
        #rospy.Subscriber("localisation", localisation, get_localisation)

        i = 1
        #print_wps(platform_full)

        if(full_strat):
            if (verbose):
                rospy.loginfo("Going randomly for a bottle")
            try_n_get_one()
            if (verbose):
               rospy.loginfo("Going for the pattern")
            #go_through_wps(way_points, GENERAL_TIME_LIMIT)
            go_through_wps(grass_to, grass_time_limit)
            go_through_wps(home_to, GENERAL_TIME_LIMIT)
            drop_bottles()

            go_through_wps(cross_to, cross_time_limit)
            go_through_wps(home_to, GENERAL_TIME_LIMIT)
            drop_bottles()

            go_through_check(platform_full, platform_time_limit, home_pt)
            go_through_check(home_to, GENERAL_TIME_LIMIT, update_localisation())
            drop_bottles()

        rospy.spin()

        #for i in range(0,2):
        #    cpx, cpy, ca = get_localisation()
        #    rospy.loginfo("CURRENT LOCA: X: "+str(cpx)+" Y: "+str(cpy)+ "A: "+str(ca))
        #    time.sleep(0.5)

#def go_to_angle(tpx, tpy, ta, dist_threshold, time_limit):
        #go_to_angle(7,7,math.pi, THRESHOLD, 10)
#        while not rospy.is_shutdown():
#            rospy.loginfo("==============================MAIN LOOP=================================")
#            rate.sleep()

#        if(i == 1):
#            cpx, cpy, ca = go_to_angle(0.35, 3, -math.pi/2, TIMEOUT, 0.1)
#            #cpx, cpy, ca = get_localisation()
#            rospy.loginfo("CURRENT LOCA: X: "+str(cpx)+" Y: "+str(cpy)+ "A: "+str(ca))
#            i = 2
       #     if(i == 1):
       #         #turn(2*math.pi)
       #         i = 2

    except rospy.ROSInterruptException:
        pass
