#!/usr/bin/env python3
import numpy as np
import cv2 as cv
import math

ARENA_X = 8
ARENA_Y = 8
DEBUG = False 

def vec_to_angle(vec,center):
    angle = math.atan2(-(center[1]-vec[1][1]),center[0]-vec[1][0])
    return angle

def vec_angle_diff(beacons,index_1,index_2,center):
    return angle_diff(vec_to_angle(beacons[index_1],center),
                      vec_to_angle(beacons[index_2],center))
#    return min(abs(vec_to_angle(beacons[index_1],center)-vec_to_angle(beacons[index_2],center)),
#               abs(vec_to_angle(beacons[index_1],center)-vec_to_angle(beacons[index_2],center)+math.pi*2),
#               abs(vec_to_angle(beacons[index_1],center)-vec_to_angle(beacons[index_2],center)-math.pi*2))
def angle_diff(a1, a2):
    return min(abs(a1-a2),
               abs(a1-a2+math.pi*2),
               abs(a1-a2-math.pi*2))

def add_angle_avg(angle, angle_x, angle_y):
    if (angle is None):
        return (angle_x, angle_y)
    else:
        angle_x = angle_x + math.cos(angle)
        angle_y = angle_y + math.sin(angle)
        return (angle_x, angle_y)

def remove_outsider(angles):
    ANGLE_DIFF_THRESHOLD = 0.1
    diffs = []
    for a1 in angles:
        total_diff = avg_diff = 0
        nb_diffs = 0
        for a2 in angles:
            if (a1 is not None and a2 is not None):
                total_diff += angle_diff(a1, a2)
                nb_diffs += 1
        if (nb_diffs > 0):
            avg_diff = total_diff/nb_diffs
        diffs.append(avg_diff)
    max_diff = max(diffs)
    if (max_diff > ANGLE_DIFF_THRESHOLD):
        index = diffs.index(max_diff)
        angles[index] = None
        return angles
    else:
        return angles 

def beacons_to_angle(beacons,center,pos):
    count = 0    #will be used for the circular mean
    angle_x = angle_y = 0
    angle_1 = angle_2 = angle_3 = angle_4 = None
    if beacons[0] is not None:
        count = count + 1
        red_angle = vec_to_angle(beacons[0],center)
        #if red_angle < -180:
        #    red_angle = red_angle + math.radians(360)
        #angle_red = red_angle-math.atan2(pos[1],ARENA_X-pos[0])
        #print(math.atan2(pos[1],pos[0]))
        angle_1 = math.atan2(pos[1],pos[0]-ARENA_X)+red_angle-math.pi
        #print(angle_1)
    if beacons[1] is not None:
        count = count + 1
        green_angle = vec_to_angle(beacons[1], center)
        #if red_angle < -180:
        #    red_angle = red_angle + math.radians(360)
        #angle = red_angle - math.radians(135)
        #print(math.atan2(pos[1], pos[0]))
        #print(green_angle)
        angle_2 =  math.atan2(pos[1], pos[0]) + green_angle -math.pi
        #print(angle_2)
    if beacons[3] is not None:
        count = count+1
        purple_angle = vec_to_angle(beacons[3], center)
        angle_4 = math.atan2(pos[1]-ARENA_Y,pos[0]- ARENA_X) + purple_angle +math.pi
        #print(angle_4)
    if beacons[2] is not None:
        count = count+1
        blue_angle = vec_to_angle(beacons[2], center)
        angle_3 = math.atan2(pos[1]-ARENA_Y,pos[0]) + blue_angle -math.pi
        #print(angle_3)
    if count == 0:
        return None

    angles = [angle_1, angle_2, angle_3, angle_4]
    angles = remove_outsider(angles)
    for a in angles:
        angle_x, angle_y = add_angle_avg(a, angle_x, angle_y)
    return math.atan2(angle_y/count,angle_x/count)


def beacons_to_pos(beacons,center):
    pos = None
    pos1 = None
    pos2 = None
    pos3 = None
    pos4 = None
    if beacons[0] is not None and beacons[1] is not None and beacons[2] is not None:
        alpha = vec_angle_diff(beacons,0,1,center)
        beta = vec_angle_diff(beacons,1,2,center)
        pos1 = angles_to_pos(alpha,beta,"red-green-blue")
        if(DEBUG):
            print("position triangulated from red-green-blue =",pos1)
        if(is_valid_pos(pos1)):
            pos = pos1
    if beacons[1] is not None and beacons[2] is not None and beacons[3] is not None:
        alpha = vec_angle_diff(beacons,1,2,center)
        beta = vec_angle_diff(beacons,2,3,center)
        pos2 = angles_to_pos(alpha, beta, "green-blue-purple")
        if(DEBUG):
            print("position triangulated from green-blue-purple", pos2)
        if(is_valid_pos(pos2)):
            pos = pos2
    if beacons[0] is not None and beacons[2] is not None and beacons[3] is not None:
        alpha = vec_angle_diff(beacons,2,3,center)
        beta = vec_angle_diff(beacons,3,0,center)
        pos3 = angles_to_pos(alpha, beta, "blue-purple-red")
        if(DEBUG):
            print("position triangulated from blue-purple-red", pos3)
        if(is_valid_pos(pos3)):
            pos = pos3
    if beacons[0] is not None and beacons[1] is not None and beacons[3] is not None:
        alpha = vec_angle_diff(beacons,3,0,center)
        beta = vec_angle_diff(beacons,0,1,center)
        pos4 = angles_to_pos(alpha, beta, "purple-red-green")
        if(DEBUG):
            print("position triangulated from purple-red-green", pos4)
        if(is_valid_pos(pos4)):
            pos = pos4
    if beacons[0] is not None and beacons[1] is not None and beacons[2] is not None and beacons[3] is not None:
        sum_ = (0,0)
        nb_pos = 0
        sum_, nb_pos = add_pos(sum_, pos1, nb_pos)
        sum_, nb_pos = add_pos(sum_, pos2, nb_pos)
        sum_, nb_pos = add_pos(sum_, pos3, nb_pos)
        sum_, nb_pos = add_pos(sum_, pos4, nb_pos)
        return (sum_[0])/nb_pos,(sum_[1])/nb_pos
    else:
        return pos

def add_pos(sum_, pos, nb_pos):
    if (is_valid_pos(pos)):
        return (sum_[0] + pos[0], sum_[1] + pos[1]), (nb_pos + 1)
    else:
        return sum_, nb_pos

def is_valid_pos(pos):
    return ((pos[0] >= 0 and pos[0] <= ARENA_X) and
           (pos[1] >= 0 and pos[1] <= ARENA_Y))

def angles_to_pos(alpha,beta,trig_type):
    if trig_type == "red-green-blue" or trig_type == "blue-purple-red":
        coeff = ARENA_X/ARENA_Y
    elif trig_type == "green-blue-purple" or trig_type == "purple-red-green":
        coeff = ARENA_Y/ARENA_X
    else:
        if(DEBUG):
            print("invalid triangulation type")
        return None
    theta = math.atan(math.sin(math.radians(270)-alpha-beta)/(math.cos(math.radians(270)-alpha-beta)+coeff*math.sin(beta)/math.sin(alpha)))
    theta = abs(theta)
    psi = math.radians(180)-alpha-theta
    #print(psi)
    #print(alpha)
    #print(beta)
    if trig_type == "red-green-blue":
        ref = (0,0)
        return ref[0]+math.cos(psi)*math.sin(theta)*ARENA_X/math.sin(alpha),ref[1]+math.sin(psi)*math.sin(theta)*ARENA_X/math.sin(alpha)
    elif trig_type == "green-blue-purple":
        ref = (0,ARENA_Y)
        return ref[0]+math.sin(psi) * math.sin(theta) * ARENA_Y / math.sin(alpha),ref[1]- math.cos(psi) * math.sin(
            theta) * ARENA_Y / math.sin(alpha)
    elif trig_type == "blue-purple-red":
        ref = (ARENA_X,ARENA_Y)
        return ref[0]-math.cos(psi) * math.sin(theta) * ARENA_X / math.sin(alpha), ref[1]-math.sin(psi) * math.sin(
            theta) * ARENA_X / math.sin(alpha)
    elif trig_type == "purple-red-green":
        ref = (ARENA_X,0)
        return ref[0]-math.sin(psi) * math.sin(theta) * ARENA_Y / math.sin(alpha), ref[1]+math.cos(psi) * math.sin(
            theta) * ARENA_Y / math.sin(alpha)

def img_to_pos(img):
    #img = cv.imread('images/tri1.png', cv.IMREAD_COLOR)

    if (DEBUG):
        print("ENTERED FUNCTION")
    find_center = cv.inRange(img,np.array([0,0,0],np.uint8),np.array([25,25,25],np.uint8))
    if (DEBUG):
        print("T1")
        #cv.imshow("oui",find_center)
    find_center_blurred = cv.GaussianBlur(find_center,(5,5),0)
    if (DEBUG):
        print("T1.5")

    #circle=cv.HoughCircles(find_center_blurred,cv.HOUGH_GRADIENT,1,5)
    circle = None
    if (DEBUG):
        print("T1.75")

    #img.shape: 1200,675
    if circle:
        center = (circle[0][0][0],circle[0][0][0])
    else:
        center = (img.shape[1]*(0.485),img.shape[0]*(0.49))
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    hue = hsv[:,:,0]
    sat = hsv[:,:,1]
    if (DEBUG):
        print("T1.9")

    #cv.imshow("og",img)
    #cv.imshow("image hue", hue)
    #cv.imshow("image sat", sat)
    #cv.imshow("image val", hsv[:,:,2])
    
    kernel = np.ones((5,5),np.uint8)
    #crop image
    mask1 = cv.inRange(hsv,np.array([0, 0, 0],np.uint8), np.array([0, 0, 0],np.uint8))
    mask1 = cv.circle(mask1, (math.ceil(center[0]),math.ceil(center[1])), math.ceil(center[0]/1.7), (255,255,255), -1)
    if (DEBUG):
        cv.imshow("circle mask", mask1)
        print("T2")

    MIN_RED = np.array([0, 60, 160],np.uint8)
    MAX_RED = np.array([42, 150, 255],np.uint8)
    MIN_RED_UP = np.array([155, 60, 160],np.uint8)
    MAX_RED_UP = np.array([180, 150, 255],np.uint8)
    MIN_GREEN = np.array([60, 60, 200],np.uint8) 
    MAX_GREEN = np.array([88, 140, 255],np.uint8) 
    MIN_BLUE = np.array([95, 140, 200],np.uint8)
    MAX_BLUE = np.array([125, 255, 255],np.uint8)
    MIN_PURPLE = np.array([135, 100, 110],np.uint8)
    MAX_PURPLE = np.array([150, 180, 255],np.uint8)
    # threshold colored circles
    mask_red_uncropped = cv.bitwise_or(cv.inRange(hsv,MIN_RED, MAX_RED),cv.inRange(hsv,MIN_RED_UP,MAX_RED_UP))
    mask_green_uncropped = cv.inRange(hsv,MIN_GREEN, MAX_GREEN)
    mask_blue_uncropped = cv.inRange(hsv,MIN_BLUE,MAX_BLUE)
    mask_purple_uncropped = cv.inRange(hsv,MIN_PURPLE,MAX_PURPLE)
    mask_red = cv.bitwise_and(mask_red_uncropped,mask_red_uncropped, mask = mask1)
    mask_green = cv.bitwise_and(mask_green_uncropped,mask_green_uncropped, mask = mask1)
    mask_blue = cv.bitwise_and(mask_blue_uncropped,mask_blue_uncropped, mask = mask1)
    mask_purple = cv.bitwise_and(mask_purple_uncropped,mask_purple_uncropped, mask = mask1)

    if (DEBUG):
        print("T3")

    if (DEBUG):
        cv.imshow("HSV red ranged", mask_red)
        cv.imshow("HSV green ranged", mask_green)
        cv.imshow("HSV blue ranged", mask_blue)
        cv.imshow("HSV purple ranged", mask_purple)

    kernel2 = cv.getStructuringElement(cv.MORPH_ELLIPSE,(3,3))
    kernel3 = cv.getStructuringElement(cv.MORPH_ELLIPSE,(5,5))
    test = cv.bitwise_and(cv.Canny(sat,10,40),cv.Canny(sat,10,40), mask = cv.morphologyEx(mask_red, cv.MORPH_DILATE, kernel3))
    test_green = cv.bitwise_and(cv.Canny(hue,20,30),cv.Canny(hue,20,30), mask = cv.morphologyEx(mask_green, cv.MORPH_DILATE, kernel3))
    test_blue = cv.bitwise_and(cv.Canny(hue,20,30),cv.Canny(hue,20,30), mask = cv.morphologyEx(mask_blue, cv.MORPH_DILATE, kernel3))
    test_purple = cv.bitwise_and(cv.Canny(hue,20,30),cv.Canny(hue,20,30), mask = cv.morphologyEx(mask_purple, cv.MORPH_DILATE, kernel3))
    #cv.imshow("Canny'd red", test)
    #cv.imshow("Canny'd green", test_green)
    #cv.imshow("Canny'd blue", test_blue)
    #cv.imshow("Canny'd purple", test_purple)

    if (DEBUG):
        print("T4")

    lines = cv.HoughLinesP(test, rho=1, theta=math.pi/360,threshold=5,minLineLength=10, maxLineGap=40)
    lines_green = cv.HoughLinesP(test_green, rho=1, theta=math.pi/360,threshold=5,minLineLength=10, maxLineGap=40)
    lines_blue = cv.HoughLinesP(test_blue, rho=1, theta=math.pi/360,threshold=5,minLineLength=10, maxLineGap=40)
    lines_purple = cv.HoughLinesP(test_purple, rho=1, theta=math.pi/360,threshold=5,minLineLength=10, maxLineGap=40)
    line_image = np.copy(img)
    cv.circle(line_image,(math.ceil(center[0]),math.ceil(center[1])), math.ceil(center[0]/3.5),(255,255,255),3)

    beacons = [None,None,None,None]
    if (DEBUG):
        print("T5")

    if lines is not None:
        result = [(0, 0), (center[0],center[1])]
        i = 0
        for line in lines:
            for x1,y1,x2,y2 in line:
                i = i+1
                x1c = math.ceil(math.sqrt((math.pow((center[0])-x1,2)+math.pow((center[1])-y1,2))))
                x2c = math.ceil(math.sqrt((math.pow((center[0])-x2,2)+math.pow((center[1])-y2,2))))
                closest = (x1,y1)
                farthest = (x2,y2)
                if x1c > x2c:
                    closest = (x2,y2)
                    farthest = (x1, y1)
                xc = (math.ceil(center[0])-closest[0], math.ceil(center[1])-closest[1])
                angle_to_center = math.atan2(xc[1],xc[0])
                angle_vector = math.atan2(closest[1]-farthest[1],closest[0]-farthest[0])
                if abs(angle_to_center-angle_vector) > math.radians(25):
                    if(DEBUG):
                        print("deleted a red line. reason: illegal angle")
                    lines = np.delete(lines, i - 1, 0)
                    i = i - 1
                else:
                    if i == 1:
                        result[0] = closest
                        result[1] = farthest
                    else:
                        if math.sqrt((math.pow((center[0]) - closest[0], 2) + math.pow((center[1]) - closest[1],
                                                                                       2))) < math.sqrt(
                                (math.pow((center[0]) - result[0][0], 2) + math.pow((center[1]) - result[0][1], 2))):
                            result[0] = closest
                        if math.sqrt((math.pow((center[0]) - farthest[0], 2) + math.pow((center[1]) - farthest[1],
                                                                                        2))) > math.sqrt(
                                (math.pow((center[0]) - result[1][0], 2) + math.pow((center[1]) - result[1][1], 2))):
                            result[1] = farthest
                    cv.line(line_image,(x1,y1),(x2,y2),(0,0,255),5)
        if lines.size>0:
            cv.line(line_image,result[0],result[1],(0,0,0),2)
            beacons[0]= result
        if (DEBUG):
            print("T6")

    else:
        if(DEBUG):
            print("no red line found bruh")

    if lines_green is not None:
        result = [(0, 0), (center[0],center[1])]
        i = 0
        for line in lines_green:
            for x1,y1,x2,y2 in line:
                i = i + 1
                x1c = math.ceil(math.sqrt((math.pow((center[0]) - x1, 2) + math.pow((center[1]) - y1, 2))))
                x2c = math.ceil(math.sqrt((math.pow((center[0]) - x2, 2) + math.pow((center[1]) - y2, 2))))
                closest = (x1, y1)
                farthest = (x2, y2)
                if x1c > x2c:
                    closest = (x2, y2)
                    farthest = (x1, y1)
                xc = (math.ceil(center[0]) - closest[0], math.ceil(center[1]) - closest[1])
                angle_to_center = math.atan2(xc[1], xc[0])
                angle_vector = math.atan2(closest[1] - farthest[1], closest[0] - farthest[0])
                if abs(angle_to_center - angle_vector) > math.radians(25):
                    if(DEBUG):
                        print("deleted a green line. reason: illegal angle")
                    lines_green = np.delete(lines_green, i-1,0)
                    i = i-1
                else:
                    if i == 1:
                        result[0] = closest
                        result[1] = farthest
                    else:
                        if math.sqrt((math.pow((center[0]) - closest[0], 2) + math.pow((center[1]) - closest[1],
                                                                                       2))) < math.sqrt(
                                (math.pow((center[0]) - result[0][0], 2) + math.pow((center[1]) - result[0][1], 2))):
                            result[0] = closest
                        if math.sqrt((math.pow((center[0]) - farthest[0], 2) + math.pow((center[1]) - farthest[1],
                                                                                        2))) > math.sqrt(
                                (math.pow((center[0]) - result[1][0], 2) + math.pow((center[1]) - result[1][1], 2))):
                            result[1] = farthest
                    cv.line(line_image, (x1, y1), (x2, y2), (0,255,0), 5)
        if lines_green.size>0:
            cv.line(line_image,result[0],result[1],(0,0,0),2)
            beacons[1]= result
    else:
        if(DEBUG):
            print("no green line found bruh")

    if lines_blue is not None:
        result = [(0, 0), (center[0],center[1])]
        i = 0
        for line in lines_blue:
            for x1,y1,x2,y2 in line:
                i = i + 1
                x1c = math.ceil(math.sqrt((math.pow((center[0]) - x1, 2) + math.pow((center[1]) - y1, 2))))
                x2c = math.ceil(math.sqrt((math.pow((center[0]) - x2, 2) + math.pow((center[1]) - y2, 2))))
                closest = (x1, y1)
                farthest = (x2, y2)
                if x1c > x2c:
                    closest = (x2, y2)
                    farthest = (x1, y1)
                xc = (math.ceil(center[0]) - closest[0], math.ceil(center[1]) - closest[1])
                angle_to_center = math.atan2(xc[1], xc[0])
                angle_vector = math.atan2(closest[1] - farthest[1], closest[0] - farthest[0])
                if abs(angle_to_center - angle_vector) > math.radians(25):
                    if(DEBUG):
                        print("deleted a blue line. reason: illegal angle")
                    lines_blue = np.delete(lines_blue, i - 1, 0)
                    i = i - 1
                else:
                    if i == 1:
                        result[0] = closest
                        result[1] = farthest
                    else:
                        if math.sqrt((math.pow((center[0]) - closest[0], 2) + math.pow((center[1]) - closest[1],
                                                                                       2))) < math.sqrt(
                                (math.pow((center[0]) - result[0][0], 2) + math.pow((center[1]) - result[0][1], 2))):
                            result[0] = closest
                        if math.sqrt((math.pow((center[0]) - farthest[0], 2) + math.pow((center[1]) - farthest[1],
                                                                                        2))) > math.sqrt(
                                (math.pow((center[0]) - result[1][0], 2) + math.pow((center[1]) - result[1][1], 2))):
                            result[1] = farthest
                    cv.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 5)
        if lines_blue.size>0:
            cv.line(line_image,result[0],result[1],(0,0,0),2)
            beacons[2]= result
    else:
        if(DEBUG):
            print("no blue line found bruh")
    if lines_purple is not None:
        result = [(0, 0), (center[0],center[1])]
        i = 0
        for line in lines_purple:
            for x1,y1,x2,y2 in line:
                i = i + 1
                x1c = math.ceil(math.sqrt((math.pow((center[0]) - x1, 2) + math.pow((center[1]) - y1, 2))))
                x2c = math.ceil(math.sqrt((math.pow((center[0]) - x2, 2) + math.pow((center[1]) - y2, 2))))
                closest = (x1, y1)
                farthest = (x2, y2)
                if x1c > x2c:
                    closest = (x2, y2)
                    farthest = (x1, y1)
                xc = (math.ceil(center[0]) - closest[0], math.ceil(center[1]) - closest[1])
                angle_to_center = math.atan2(xc[1], xc[0])
                angle_vector = math.atan2(closest[1] - farthest[1], closest[0] - farthest[0])
                if abs(angle_to_center - angle_vector) > math.radians(25):
                    if(DEBUG):
                        print("deleted a purple line. reason: illegal angle")
                    lines_purple = np.delete(lines_purple, i - 1, 0)
                    i = i - 1
                else:
                    if i == 1:
                        result[0] = closest
                        result[1] = farthest
                    else:
                        if math.sqrt((math.pow((center[0]) - closest[0], 2) + math.pow((center[1]) - closest[1],
                                                                                       2))) < math.sqrt(
                                (math.pow((center[0]) - result[0][0], 2) + math.pow((center[1]) - result[0][1], 2))):
                            result[0] = closest
                        if math.sqrt((math.pow((center[0]) - farthest[0], 2) + math.pow((center[1]) - farthest[1],
                                                                                        2))) > math.sqrt(
                                (math.pow((center[0]) - result[1][0], 2) + math.pow((center[1]) - result[1][1], 2))):
                            result[1] = farthest
                    cv.line(line_image, (x1, y1), (x2, y2), (255, 0, 255), 5)
        if lines_purple.size > 0:
            cv.line(line_image, result[0], result[1], (0, 0, 0), 2)
            beacons[3]= result
    else:
        if(DEBUG):
            print("no blue line found bruh")
            print("beacons",beacons)
    new_beacons = []
#    MAX_ANGLE_TO_CENTER = 35
    for beacon in beacons:
        if(beacon is not None):
#            beacon_angle_to_center = vec_to_angle(beacon, center)
#            xc = (math.ceil(beacon[1][0]) - beacon[0][0], math.ceil(beacon[1][1]) - beacon[0][1])
#            beacon_angle = math.atan2(xc[1], xc[0])
#            if (abs(beacon_angle_to_center) - abs(beacon_angle) > math.radians(MAX_ANGLE_TO_CENTER)):
#                print("NOW deleting an illegal beacon")
            # Check if length(beacon) is greater than half the circle
            if (vec_norm(beacon) > (img.shape[1]/2)):
                beacon = None 
                if (DEBUG):
                    print("NOW deleting an illegal beacon")
        new_beacons.append(beacon)
    beacons = new_beacons

    loca = beacons_to_pos(beacons,center)
    position = None
    if loca is not None:
        position = loca[0], loca[1], beacons_to_angle(beacons,center,loca)
    else:
        position = None, None, None
    #print("position",position)
    if (DEBUG):
        cv.imshow("lines", line_image)
        cv.waitKey(0)
        cv.destroyAllWindows()
        WHITE_BALANCE = str(4)
        cv.imwrite("/home/frirobot/ros_catkin_ws/src/fri_robot/scripts/img_wb_"+WHITE_BALANCE+".jpg", line_image)

    return position

def vec_norm(vec):
    return math.sqrt(
            math.pow(vec[0][0]-vec[1][0], 2) +
            math.pow(vec[0][0]-vec[1][0], 2)
           )

#if __name__ == "__main__":
    #picture = cv.imread('test.jpg', cv.IMREAD_COLOR)
    #picture = cv.resize(picture,(1200,675))
    #cv.imshow("img", picture)
    #cv.waitKey(0)
    #cv.destroyAllWindows()
    #posX, posY, ang = img_to_pos(picture)
    #print(posX, posY, ang)
