#!/usr/bin/env python
import rospy
import math
from std_msgs.msg import Bool
from vision_msgs.msg  import Detection2DArray 
from std_msgs.msg import Float64MultiArray
from fri_robot.msg import BottlePosition as bottlePosition

def compute_distance(size_x, size_y, FL):
    min_size = min(size_x, size_y)
    BOTTLE_DIAMETER = 63
    distance = FL * BOTTLE_DIAMETER / min_size

    return distance

def compute_fl(size_x, size_y):
    min_size = min(size_x, size_y)
    BOTTLE_DIAMETER = 63
    distance = 297
    fl = distance * min_size / BOTTLE_DIAMETER

    return fl 

def compute_angle(distance, center_x, FL, IW):
    w = distance * IW / FL
    dx = (center_x - (IW/2)) / IW * w
    return -math.asin(dx/distance)

def callback(data):

    verbose = False
    MIN_SCORE = 0.6
    MAX_DIST = 2000
    detections = data.detections
    nb_det = len(detections)
    bottle_pos = bottlePosition()

    if(verbose):
        rospy.loginfo("detected %s bottles", nb_det)

    for det in detections:
#        rospy.loginfo("new det")
        score = det.results[0].score
        bbox = det.bbox
        center = bbox.center
        size_x = bbox.size_x
        size_y = bbox.size_y

        if(score >= MIN_SCORE):
            if(verbose):
                rospy.loginfo("============================")
                rospy.loginfo("score: %s", score)
                rospy.loginfo("center: %s", center)
                rospy.loginfo("size: %s x %s", size_x, size_y)

            FOCAL_LENGTH = 1700 
            IMAGE_WIDTH = 1280
            distance = compute_distance(size_x, size_y, FOCAL_LENGTH)
            angle = compute_angle(distance, center.x, FOCAL_LENGTH, IMAGE_WIDTH)
            # fl = compute_fl(size_x, size_y)
            # rospy.loginfo("estimated fl: %s", fl)
#            rospy.loginfo("computed distance: %s", distance)
#            rospy.loginfo("computed angle: %s", angle)
            bottle_pos.distance = distance
            bottle_pos.angle = angle

            if(verbose):
                rospy.loginfo("=<<<<<<<<<<<<<<<<<<<<<<<<<<=")
            if(distance < MAX_DIST):
                position_pub.publish(bottle_pos)

if __name__ == '__main__':
    position_pub = rospy.Publisher('bottle_position', bottlePosition, queue_size=10)
    ask = True
    detection_asker = rospy.Publisher('frame_asking', Bool, queue_size=10)
    rospy.init_node('bottle_position', anonymous=False)
    rospy.Subscriber("/detectnet/detections", Detection2DArray, callback)
    rate = rospy.Rate(0.5)
    while not rospy.is_shutdown():
        detection_asker.publish(ask)
        rate.sleep()

    # spin() simply keeps python from exiting until this node is stopped

