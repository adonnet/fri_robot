#!/usr/bin/env python3
import rospy
from std_msgs.msg import Bool
from fri_robot.msg import Localisation as localisation

import cv2 as cv
import numpy as np
import math
import random
import time

import localisation_helper as loc_helper
from fri_robot.srv import GetLocalisation, GetLocalisationResponse

def define_video_object():
    WHITE_BALANCE = str(4)
    MIN_EXP_TIME = str(5000)
    MAX_EXP_TIME = str(5000)

    camSet='nvarguscamerasrc ee-mode=2 ' \
           'sensor_id=1 aelock=True wbmode='+WHITE_BALANCE+ ' exposuretimerange="'+MIN_EXP_TIME+' '+MAX_EXP_TIME+'"!  ' \
           'video/x-raw(memory:NVMM), width=1920, height=1080, format=NV12, framerate=30/1 ! ' \
           'nvvidconv flip-method=0 ! ' \
           'video/x-raw, width=1920, height=1080, format=BGRx ! ' \
           'videoconvert ! video/x-raw, format=BGR ! appsink'
    return cv.VideoCapture(camSet,cv.CAP_GSTREAMER)

def cam_loca_to_center(x, y, angle):
    if(x is None or y is None or angle is None):
        return x, y, angle
    else:
        cpx = cpy = 0
        CENTER_TO_CAM_X = 0.28
        CENTER_TO_CAM_Y = 0
        cpx = x + math.cos(angle) * CENTER_TO_CAM_X
        cpy = y + math.sin(angle) * CENTER_TO_CAM_X
        return cpx, cpy, angle

def loca_from_video_object(video_object, i):
    # get the last grabbed frame
    captured = video_object.retrieve(img)
    if captured:
        if(debug):
            tag = random.randint(0, 10000000)
            cv.imwrite("/home/frirobot/ros_catkin_ws/src/fri_robot/scripts/img_wb_"+str(tag)+"_moving.jpg", img)
            rospy.loginfo("CORRESPONDING TAG: "+str(tag))


        # Compute localisation
        loc = GetLocalisationResponse()
        cam_x, cam_y, cam_angle =  loc_helper.img_to_pos(img)
        #if(debug):
        #    rospy.loginfo("CURRENT CAM LOCA: X: "+str(cam_x)+" Y: "+str(cam_y)+ "A: "+str(cam_angle))
        # Adjust localisation between the wheels instead of on camera
        loc.pos_x, loc.pos_y, loc.angle = cam_loca_to_center(cam_x, cam_y, cam_angle)

        # Return new localisation
        if (not loc.pos_x):
            loc.pos_x = loc.pos_y = loc.angle = -1
        return loc

def get_localisation(req):
    return loca_from_video_object(video_object, req)

if __name__ == "__main__":
    NB_PICS_TO_SET_UP = 50
    debug = False 
    video_object = define_video_object()
    capture, img = video_object.read()
    for i in range(0, NB_PICS_TO_SET_UP):
        video_object.grab()

    rospy.init_node('localisation_service', anonymous=False)
    localisation_service = rospy.Service('compute_localisation', GetLocalisation, get_localisation)

    while not rospy.is_shutdown():
        video_object.grab()

    rospy.spin()
