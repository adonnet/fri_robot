#!/usr/bin/env python
import serial
import time
import rospy
from std_msgs.msg import Bool
from std_msgs.msg import Float64
from fri_robot.msg import MotorSpeed as motorSpeed

def motor_callback(data):
    motor_speed(data.left_speed, data.right_speed)

def lift_callback(data):
    print("==>lift")
    gatherBottle(10)

#duty should be between 10-90  (it corresponds to 0-10'000 rpm). Values could be negative and float 
def motor_speed(l_duty, r_duty):
	try_ctr = 0
	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
		while try_ctr<10:
			msg = "ConstMotor " + str(l_duty) +" "+ str(r_duty)
			checksum = 0			
			for x in msg:
				checksum += ord(x)
			msg += " " + str(checksum) + "\n"
			ser.write(bytes(msg))
			line = ser.readline()
			if "OK" in line:
				break
			try_ctr = try_ctr + 1
			time.sleep(.01)
	if try_ctr < 10 :
		if (debug):
			print("Success: motor command sent\n")
	else:
		if (debug):
			print("Failed: motor command not sent\n")

#duty should be between 10-90  (it corresponds to 0-10'000 rpm). Values could be negative and float 
# duration is in ms
def timed_motor_speed(l_duty, r_duty, duration):
	rospy.loginfo("DURATION: "+str(duration))
	try_ctr = 0
	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
		while try_ctr<10:
			msg = "TimedMotor " + str(l_duty) +" "+ str(r_duty) + " " + str(duration)
			checksum = 0			
			for x in msg:
				checksum += ord(x)
			msg += " " + str(checksum) + "\n"
			ser.write(bytes(msg))
			line = ser.readline()
			if "OK" in line:
				break
			try_ctr = try_ctr + 1
			time.sleep(0.1)
	if try_ctr < 10 :
		print("Success: motor command sent\n")
	else:
		print("Failed: motor command not sent\n")

#set rake position
#angle should be between 0 and 270. It could be double. However the change of angle is 1 degree per delay. Angle is according to servo2
def rake_angle(angle,delay):
	try_ctr = 0
	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
		while try_ctr<10:
			msg = "Rake " + str(angle) +" "+ str(delay)
			checksum = 0			
			for x in msg:
				checksum += ord(x)
			msg += " " + str(checksum) + "\n"
			ser.write(bytes(msg))
			line = ser.readline()
			if "OK" in line:
				break
			try_ctr = try_ctr + 1
			time.sleep(.01)
	if try_ctr < 10 :
		print("Success: rake command sent\n")
	else:
		print("Failed: rake command not sent\n")

#set bucket position
#angle should be between 0 and 270. It could be double. However the change of angle is 1 degree per delay
def bucket_angle(angle,delay):
	try_ctr = 0
	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
		while try_ctr<10:
			msg = "Bucket " + str(angle) +" "+ str(delay)
			checksum = 0			
			for x in msg:
				checksum += ord(x)
			msg += " " + str(checksum) + "\n"
			ser.write(bytes(msg))
			line = ser.readline()
			if "OK" in line:
				break
			try_ctr = try_ctr + 1
			time.sleep(.01)
	if try_ctr < 10 :
		print("Success: bucket command sent\n")
	else:
		print("Failed: bucket command not sent\n")

#set arm angle
#angle should be between 0 and 300. It could be double.
def arm_angle(angle):
	try_ctr = 0
	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
		while try_ctr<10:
			msg = "ArmAngle " + str(angle)
			checksum = 0			
			for x in msg:
				checksum += ord(x)
			msg += " " + str(checksum) + "\n"
			ser.write(bytes(msg))
			line = ser.readline()
			if "OK" in line:
				break
			try_ctr = try_ctr + 1
			time.sleep(.01)
	if try_ctr < 10 :
		print("Success: arm command sent\n")
	else:
		print("Failed: arm command not sent\n")

#duration in millisecond
def open_backdoor(duration):
	try_ctr = 0
	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
		while try_ctr<10:
			msg = "OpenBackdoor " + str(duration)
			checksum = 0			
			for x in msg:
				checksum += ord(x)
			msg += " " + str(checksum) + "\n"
			ser.write(bytes(msg))
			line = ser.readline()
			if "OK" in line:
				break
			try_ctr = try_ctr + 1
			time.sleep(.01)
	if try_ctr < 10 :
		print("Success: backdoor command sent\n")
	else:
		print("Failed: backdoor command not sent\n")

#get Range sensors values
#return a list with the value of the sensors. List starts with the IR sensors, then the US sensors. 
#Values are brut measurements: adc 12 bit from IR sensors and time difference between rising and falling edge for US Sensors (in microseconds)
#If the range values could not be determined, it returns -1
def getRSensors():
	header = "Range"
	line = " "
	value_list = []
	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
		while line[:len(header)] != header:
			line = ser.readline()
	splitted  = line.split(" ")
	splitted.pop(0)
	splitted.pop(-1)
	for x in splitted:
		if x.isdigit():
			value_list.append(int(x))
		else:
			return -1
	return value_list

#IR_to_cm
#Distance (cm) = 4.8/(VoltageRatio - 0.02)
#Return if the distance is greater than 80
def IR_to_cm(IR_value):
	if (IR_value<320):
		return -1
	else:
		return 4.8/(IR_value/4096. - 0.02)

#US_to_mm
#Distance = (Speed x Time) / 2 = (34cm/ms x Time(us)) / 2
def US_to_cm(US_value):
	return US_value * 0.034 / 2

#Prototype to gather bottle
#Try to implement it in STM32 and send command through UART
def gatherBottle(delay):
	try_ctr = 0
	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
		while try_ctr<10:
			msg = "Gather " + str(delay)
			checksum = 0			
			for x in msg:
				checksum += ord(x)
			msg += " " + str(checksum) + "\n"
			ser.write(bytes(msg))
			line = ser.readline()
			if "OK" in line:
				break
			try_ctr = try_ctr + 1
			time.sleep(.01)
	if try_ctr < 10 :
		print("Success: gather command sent\n")
	else:
		print("Failed: gather command not sent\n")

def sensor_mesures_to_msg(sensor_mesures):
    #ir_left = ir_to_cm(sensor_mesures[0])
    sensor_mesures = []
    for x in sensor_measures():
        if x<3:
            sensor_mesures.append(IR_to_cm(x))
        else:
            sensor_mesures.append(US_to_cm(x))
    return sensor_mesures


### Main: Listen to wheel speed
if __name__ == '__main__' :
    try:
        debug = False

        rospy.init_node("uart", anonymous=False)
        us_rate = rospy.Rate(0.5)
        rospy.loginfo("UART node initialized")

        rospy.Subscriber("motor_speed", motorSpeed, motor_callback, queue_size=3)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass

#while True:
#	with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
#		line = ser.readline()
#		print (line)
#		ser.write(b"T")
#		print ('Toggle LED command sent')
#	time.sleep(2)
